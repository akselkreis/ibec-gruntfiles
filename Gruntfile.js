module.exports = function(grunt) {

	// 1. All configuration goes here
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),



		sass: {
		    dist: {
				options: {
					style: 'expanded'
				},
				files: {
					"sites/all/themes/neutron/css/build/styles.css": "sites/all/themes/neutron/scss/central.scss"
				}
			}
		},

		stripmq: {
        		//Viewport options
			options: {
				width: 1024,
            		type: 'screen'
        		},
        		all: {
            		files: {
                		//follows the pattern 'destination': ['source']
                		'sites/all/themes/neutron/css/build/stripped/ie8.css': ['sites/all/themes/neutron/css/build/styles.css']
            		}
        		}
    		},

		autoprefixer: {
			options: {
				browsers: ['> 3%']
			},
			multiple_files: {
				expand: true,
				flatten: true,
				src: 'sites/all/themes/neutron/css/build/*.css',
				dest: 'sites/all/themes/neutron/css/build/prefixed/'
			}
		},

		cmq: {
		    options: {
		      log: true
		    },
		    your_target: {
		      files: {
		        'sites/all/themes/neutron/css/build/mq-d': ['sites/all/themes/neutron/css/build/prefixed/*.css']
		      }
			}
		},


		cssmin: {
			combine: {
				files: {
					'sites/all/themes/neutron/css/build/minified/styles.css': [
						'sites/all/themes/neutron/css/build/mq-d/styles.css',
						],
				},

				options:{
					report: 'min'
				}
			}
		},

		jshint: {
			options: {
				'-W014':true,
				'-W018':true,
				'-W024': true,
				'-W030':true,
				'-W032':true,
				'-W041':true,
				'-W058':true,
				'-W067':true,
				'-W099':true,
				'-W093':true,
				asi:true,
				eqnull: true,
				eqeqeq: false,
				smarttabs: true,
			},
			beforeconcat: ['js/script.js'],
			afterconcat:{
				options:{
					'-W014':true,
					'-W018':true,
					'-W024': true,
					'-W030':true,
					'-W032':true,
					'-W041':true,
					'-W058':true,
					'-W067':true,
					'-W099':true,
					'-W093':true,
					asi:true,
					smarttabs:true,
					shadow:true
				},
				files:{
					src:['sites/all/themes/neutron/js/build/production.js']
				}
			}
		},


		concat: {
			dist: {
				src: [
				'sites/all/themes/neutron/js/libs/fastclick.js',
				'sites/all/themes/neutron/js/libs/modernizr.js',
				'sites/all/themes/neutron/js/libs/owl.carousel.min.js',
				'sites/all/themes/neutron/js/libs/jquery.mediaWrapper.js',
				'sites/all/themes/neutron/js/libs/jquery.captionr.js',
				'sites/all/themes/neutron/js/libs/tableit.js',
				'sites/all/themes/neutron/js/script.js'
				],
				dest: 'sites/all/themes/neutron/js/build/production.js'
			}
		},

		uglify: {
			build: {
				src: 'sites/all/themes/neutron/js/build/production.js',
				dest: 'sites/all/themes/neutron/js/build/production.min.js'
			}
		},


		watch: {
			options: {
				//livereload: true,
			},

			scripts: {
				files: ['sites/all/themes/**/*.js'],
				//files: ['sites/all/themes/neutron/js/*.js','sites/all/themes/neutron/js/**/*.js'],
				tasks: ['jshint:beforeconcat','concat','uglify'],
				options: {
					spawn: false,
				}
			},

			css: {
				files: ['sites/all/themes/**/*.scss'],
				//files: ['sites/all/themes/neutron/scss/*.scss','sites/all/themes/neutron/scss/**/*.scss','sites/all/themes/neutron/js/**/*.css'],
				tasks: ['sass', 'autoprefixer', 'cmq', 'stripmq', 'cssmin'],
				options: {
					spawn: false,
				}
			}
		}
	});

	require('load-grunt-tasks')(grunt);

	grunt.registerTask('default', ['jshint:beforeconcat','concat', 'uglify', 'sass', 'stripmq', 'autoprefixer', 'cmq', 'cssmin', 'watch']);

	grunt.registerTask('dev', ['watch']);
};
